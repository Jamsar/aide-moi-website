[![Netlify Status](https://api.netlify.com/api/v1/badges/694ff215-b8d5-46f0-a99b-7543cbf92f9a/deploy-status)](https://app.netlify.com/sites/aide-moi-website/deploys)
[![pipeline status](https://gitlab.com/seljami/aide-moi-website/badges/master/pipeline.svg)](https://gitlab.com/seljami/aide-moi-website/commits/master)
[![coverage report](https://gitlab.com/seljami/aide-moi-website/badges/master/coverage.svg)](https://gitlab.com/seljami/aide-moi-website/commits/master)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)

# Site web de l'association Aide Moi

Site internet de l'association Aide Moi.
Association Aide-Moi, régie par la loi du 1er juillet 1901 et le décret du 16 août 1901, est une association à but non-lucratif, qui œuvre pour la bienfaisance. Elle a été fondée par des acteurs soucieux de répondre aux besoins de voyager des personnes dépendantes.

## Technologies

* Next.js 
    * URL: https://nextjs.org/
    * Librairies: Typescript
* Firebase
    * URL: https://firebase.google.com/
    * Usage: Database/Authenfication 
* Netlify
    * URL: https://www.netlify.com/
    * Usage: Hosting 

## Comment l'utiliser ?

### Développement

Après le clone du projet :

```bash
yarn
yarn dev
```

### Environments

* **Test** : Déploiement automatique sur Netlify d'une version de test après chaque merge request sur la branche 'develop'.
* **Staging** : Déploiement automatique sur Netlify d'une version de staging après chaque merge request sur la branche 'master'.
* **Production** : Déploiement au clique sur Netlify disponible après chaque merge request sur la branche 'master'.